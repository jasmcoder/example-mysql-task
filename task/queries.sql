#1
SELECT DISTINCT id, name
FROM categories
         LEFT JOIN product_categories ON product_categories.category_id = categories.id
WHERE product_categories.product_id IN ('1', '7', '11');

#2
SELECT products.id, products.name
FROM products
         LEFT JOIN product_categories ON product_categories.product_id = products.id
WHERE product_categories.category_id = '3'
UNION
SELECT products.id, products.name
FROM products
         LEFT JOIN product_categories ON product_categories.product_id = products.id
WHERE product_categories.category_id IN (SELECT id FROM categories WHERE categories.category_id = '3');

#3
SELECT categories.name, COUNT(product_id) products_count
FROM product_categories
         LEFT JOIN categories ON product_categories.category_id = categories.id
WHERE product_categories.category_id IN ('2', '1', '3')
GROUP BY product_categories.category_id;

#4
SELECT SUM(products_count) products_sum
FROM (
         SELECT COUNT(product_id) products_count
         FROM product_categories pc
         WHERE pc.category_id IN ('2', '1', '3', '9')
           AND pc.product_id NOT IN (
             SELECT sub_pc.product_id
             FROM product_categories sub_pc
             WHERE sub_pc.category_id IN ('2', '1', '3', '9')
               AND sub_pc.category_id != pc.category_id
         )
         GROUP BY category_id
     ) t1