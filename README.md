# Example MySQL Task

## Installation

Clone the project
```
$ git clone https://xjasmx@bitbucket.org/jasmcoder/example-mysql-task.git
```

#### For docker developers
Run the application with makefile:

```
$ make init
```
or
```
$ docker-compose pull
$ docker-compose build
$ docker-compose up -d
$ bash ./wait-for-mysql.sh
$ bash ./init-mysql.sh
```
#### Or go to the "task" folder and dump the database locally
```
task/dump.sql
```

### Placing test queries in
```
task/queries.sql
```




