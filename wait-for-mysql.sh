#!/bin/sh
until docker container exec -it mysql_8 mysqladmin ping -P 3306 -psecret | grep "mysqld is alive" ; do
  >&2 echo "MySQL is unavailable - waiting for it... 😴"
  sleep 1
done