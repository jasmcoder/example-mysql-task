up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-pull docker-build docker-up wait-for-mysql init-mysql

docker-up:
	docker-compose up --build -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

wait-for-mysql:
	bash ./wait-for-mysql.sh

init-mysql:
	bash ./init-mysql.sh